LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY maftest IS
END maftest;
 
ARCHITECTURE behavior OF maftest IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mafdec
    PORT(
         CLK : IN  std_logic;
         DI : IN  std_logic_vector(211 downto 0);
         DO : OUT  std_logic_vector(255 downto 0);
         oe : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal DI : std_logic_vector(211 downto 0) := (others => '0');

 	--Outputs
   signal DO : std_logic_vector(255 downto 0);
   signal oe : std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mafdec PORT MAP (
          CLK => CLK,
          DI => DI,
          DO => DO,
          oe => oe
        );
 
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   CLK_process :process
   begin
		CLK <= '0';
		wait for 1 ns;
                -- FF3F814A7E3950D8AD3F81861E3F9DD2D93F824BB641AD42F03F846D5AFC01D94E
                di<=x"FF3F814A7E3950D8AD3F81861E3F9DD2D93F824BB641AD42F03F8";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"46D5AFC01D94E0D965BDC15C02E8BD4F8F3CB0126638E3C01D8CE";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"64E2BA9219A52FF3F814A7E3950D8AD3F81861E3F9DD2D93F824B";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"B641AD42F03F846D5AFC01D94EFF3F814A7E3950D8AD3F81861E3";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"F9DD2D93F824BB641AD42F03F846D5AFC01D94EFF3F814A7E3950";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"D8AD3F81861E3F9DD2D93F824BB641AD42F03F846D5AFC01D94EF";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"F3F814A7E3950D8AD3F81861E3F9DD2D93F824BB641AD42F03F84";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
                di<=x"6D5AFC01D94E00000000000000000000000000000000000000000";
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
		CLK <= '1';
		wait for 1 ns;
		CLK <= '0';
		wait for 1 ns;
   end process;
 


END;
