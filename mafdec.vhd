library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mafdec is
    Port ( CLK : in  STD_LOGIC;
           DI : in  STD_LOGIC_VECTOR (211 downto 0);
           DO : out  STD_LOGIC_VECTOR (255 downto 0);
           oe : out std_logic);
end mafdec;

architecture a of mafdec is
  type histbuf_t is array(0 to 63) of std_logic_vector(47 downto 0);
  signal histbuf: histbuf_t;
  signal hb_ra,hb_ra2,hb_wa: std_logic_vector(5 downto 0);
  signal hb_rd,hb_wd: std_logic_vector(47 downto 0);
  signal hb_we: std_logic := '0';

  type addrmap_t is array(0 to 15) of std_logic_vector(5 downto 0);
  constant addrmap: addrmap_t := (
    "111111","111110","111101","111011","111010","111001","111000","110111",
    "101111","101110","101101","101011","011010","011001","001000","000111");
  signal ali_rec: std_logic_vector(263 downto 0);
  signal fmt2: std_logic_vector(1 downto 0):="00";
  signal tbl_a: std_logic_vector(3 downto 0);
  signal sgna,sgnb,sgnc,sgnd: std_logic_vector(7 downto 0);
  signal d1,d2,d3,d4: std_logic_vector(63 downto 0);
  signal base: std_logic_vector(5 downto 0) := "000000";

  signal fmt3: std_logic_vector(1 downto 0) := "00";
  signal e1,e2,e3,e4: std_logic_vector(11 downto 0);

  signal fmt1,fmt1p,fmt1q: std_logic_vector(1 downto 0) := "00";
  signal pos,pos1: std_logic_vector(5 downto 0) := "000000";
  signal srs: std_logic_vector(5 downto 0) := "000000";
  signal di0,di1,sl4,sl8,sl16,sl32,sl64,sl128: std_logic_vector(211 downto 0);
  signal sr64,sr128,upper: std_logic_vector(211 downto 0);
  signal sr4,sr8,sr16,sr32,mid: std_logic_vector(227 downto 0);
  signal hdr: std_logic_vector(7 downto 0);
begin
  stage1: process(CLK)
  begin
    if rising_edge(CLK) then
      di1<=di;
      di0<=di1;
      ali_rec<=(upper&x"00000000"&x"00000") or (x"00000000"&x"00000"&sr128) or (mid&x"000000000");
      if fmt1p="11" then
        fmt1p<=fmt1q;
        mid<=sr32;
        if fmt1q="01" then
          pos<=5-pos;
        else
          pos<=13-pos;
        end if;
      else
        mid<=(others=>'0');
        upper<=sl128;
        case fmt1 is
          when "00"=>
            fmt1p<=fmt1; -- no spills
          when "01"=>
            if pos>48 then
              -- spill
              pos<=53-pos;
              fmt1p<="11";
              fmt1q<=fmt1;
            else
              fmt1p<=fmt1;
              pos<=pos+5;
            end if;
          when "10"=>
            if pos>39 then
              -- spill
              pos<=53-pos;
              fmt1p<="11";
              fmt1q<=fmt1;
            else
              fmt1p<=fmt1;
              pos<=pos+13;
            end if;
          when others=>null;
        end case;
      end if;
      fmt2<=fmt1p;
    end if;
  end process;
  sl4   <= di0 when pos(0)='0' else di0(207 downto 0)&x"0";
  sl8   <= sl4 when pos(1)='0' else sl4(203 downto 0)&x"00";
  sl16  <= sl8 when pos(2)='0' else sl8(195 downto 0)&x"0000";
  sl32  <= sl16 when pos(3)='0' else sl16(179 downto 0)&x"00000000";
  sl64  <= sl32 when pos(4)='0' else sl32(147 downto 0)&x"0000000000000000";
  -- somewhat dirty hack here
  sl128 <= sl64 when pos(5)='0' else
           --sl64(83 downto 80)&di(211 downto 208)&sl64(75 downto 0)&x"0000000000000000"&x"0000000000000000" when pos="110100" else
           sl64(83 downto 0)&x"0000000000000000"&x"0000000000000000";
  hdr <= sl128(211 downto 208)&di1(211 downto 208) when pos="110100" else
         sl128(211 downto 204);
  fmt1  <= "10" when hdr=x"FF" else
           "01" when hdr(7 downto 4)=x"F" else
           "00";
  srs <= 53-pos when fmt1p="10" else
         40-pos when fmt1p="00" else
         45-pos when fmt1p="01" else
         pos when fmt1p="11" else (others=>'X');
  sr4   <= di0&x"0000" when srs(0)='0' else x"0"&di0(211 downto 0)&x"000";
  sr8   <= sr4 when srs(1)='0' else x"00"&sr4(227 downto 8);
  sr16  <= sr8 when srs(2)='0' else x"0000"&sr8(227 downto 16);
  sr32  <= sr16 when srs(3)='0' else x"00000000"&sr16(227 downto 32);
  sr64  <= sr32(227 downto 16) when srs(4)='0' else x"0000000000000000"&sr32(227 downto 80);
  sr128 <= sr64 when srs(5)='0' else x"0000000000000000"&x"0000000000000000"&sr64(211 downto 128);
  -----------------------------------------------------------------------------
  hb: process(CLK)
  begin
    if rising_edge(CLK) then
      hb_ra2<=hb_ra;
      if hb_we='1' then
        histbuf(conv_integer(hb_wa))<=hb_wd;
      end if;
    end if;
  end process;
  hb_rd<=histbuf(conv_integer(hb_ra2));
  -----------------------------------------------------------------------------
  stage2: process(CLK)
  begin
    if rising_edge(CLK) then
      fmt3<=fmt2;
      case fmt2 is
        when "00"=>
          d1<=x"000"&ali_rec(259 downto 208);
          d2<=x"000"&ali_rec(207 downto 156);
          d3<=x"000"&ali_rec(155 downto 104);
          d4<=x"000"&ali_rec(103 downto 52);
          base<=base+1;
        when "01"=>
          d1<=sgna&ali_rec(255 downto 252)&ali_rec(239 downto 188);
          d2<=sgnb&ali_rec(251 downto 248)&ali_rec(187 downto 136);
          d3<=sgnc&ali_rec(247 downto 244)&ali_rec(135 downto 84);
          d4<=sgnd&ali_rec(243 downto 240)&ali_rec(83 downto 32);
          base<=base+1;
        when "10"=>
          d1<=ali_rec(255 downto 192);
          d2<=ali_rec(191 downto 128);
          d3<=ali_rec(127 downto 64);
          d4<=ali_rec(63 downto 0);
          base<=base+1;
        when others=>
          null;
      end case;
    end if;
  end process;
  sgna<=(others=>ali_rec(255));
  sgnb<=(others=>ali_rec(251));
  sgnc<=(others=>ali_rec(247));
  sgnd<=(others=>ali_rec(243));
  tbl_a <= ali_rec(263 downto 260) when fmt2="00" else
           ali_rec(259 downto 256) when fmt2="01" else
           (others=>'X');
  hb_ra <= addrmap(conv_integer(tbl_a))+base;
  -----------------------------------------------------------------------------
  stage3: process(CLK)
  begin
    if rising_edge(CLK) then
      do<= e1&d1(51 downto 0)&
           e2&d2(51 downto 0)&
           e3&d3(51 downto 0)&
           e4&d4(51 downto 0);
      if fmt3="11" then
        oe<='0';
      else
        oe<='1';
      end if;
    end if;
  end process;
  hb_wa<=base-1;
  e1 <= d1(63 downto 52) when fmt3="10" else
        d1(63 downto 52)+hb_rd(47 downto 36);
  e2 <= d2(63 downto 52) when fmt3="10" else
        d2(63 downto 52)+hb_rd(35 downto 24);
  e3 <= d3(63 downto 52) when fmt3="10" else
        d3(63 downto 52)+hb_rd(23 downto 12);
  e4 <= d4(63 downto 52) when fmt3="10" else
        d4(63 downto 52)+hb_rd(11 downto 0);
  hb_wd<=e1&e2&e3&e4;
  hb_we<='0' when fmt3="11" else '1';
  
end a;
